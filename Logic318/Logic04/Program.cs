﻿using Logic04;

//abstractClass();
//objectClass();
//constructor();
rekursifAsc();
//rekursif();//Desc

Console.ReadKey();

static void rekursifAsc()
{
    Console.WriteLine("--Rekursif Ascending--");
    Console.Write("Masukkan input : ");
    int input = int.Parse(Console.ReadLine());
    int start = 1;
    PerulanganAsc(input, start);
}

static int PerulanganAsc(int input, int start)
{
    if (input == start)
    {
        Console.WriteLine($"{start}");
        return input;
    }

    Console.WriteLine($"{start}");
    return PerulanganAsc(input, start + 1);
}

static void rekursif()
{
    Console.WriteLine("--Rekursif Function--");
    Console.Write("Masukkan input : ");
    int input = int.Parse(Console.ReadLine());

    Perulangan(input);
}

static int Perulangan(int input)
{
    if (input == 1)
    {
        Console.WriteLine($"{input}");
        return input;
    }

    Console.WriteLine($"{input}");
    return Perulangan(input - 1);
}

static void inheritance()
{
    Console.WriteLine("--Inheritance");
    TypeMobil typeMobil = new TypeMobil();

    typeMobil.Civic();
}

static void encapsulation()
{
    Console.WriteLine("--Encapsulation--");
    PersegiPanjang pp = new PersegiPanjang();

    pp.panjang = 10;
    pp.lebar = 20;
    pp.TampilkanData();
}

static void constructor()
{
    Console.WriteLine("--Constructor--");
    Mobil mobil = new Mobil("B 1234 BC");
    string platno = mobil.getPlatNo();

    Console.WriteLine($"Mobil dengan nomor polisi : {platno}");
}

static void objectClass()
{
    Console.WriteLine("--Object Class--");
    Mobil mobil = new Mobil("") { nama = "Mitsubishi Lancer Evo VIII", kecepatan = 0, bensin=25, posisi=0};

    //Mobil mobil2 = new Mobil();
    //mobil2.nama = "Honda NSX";
    //mobil2.kecepatan = 0;
    //mobil2.bensin = 20;
    //mobil2.posisi = 0;

    mobil.percepat();
    mobil.maju();
    mobil.isiBensin(20);
    mobil.utama();
}

static void abstractClass()
{
    Console.WriteLine("--Abstract Class--");
    Console.Write("Masukkan input x : ");
    int x = int.Parse(Console.ReadLine());
    Console.Write("Masukkan output y : ");
    int y = int.Parse(Console.ReadLine());

    TestTurunan calculator = new TestTurunan();
    int jumlah = calculator.Jumlah(x, y);
    int kurang = calculator.Kurang(x,y);

    Console.WriteLine("Hasil penjumlahan x + y = " + jumlah);
    Console.WriteLine("Hasil penjumlahan x + y = " + jumlah);
}