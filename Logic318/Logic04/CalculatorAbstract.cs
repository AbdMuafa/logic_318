﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic04
{
    public abstract class CalculatorAbstract
    {
        public abstract int Jumlah(int x, int y);
        public abstract int Kurang(int x, int y);
    }

    public class TestTurunan : CalculatorAbstract
    {
        public override int Jumlah(int x, int y)
        {
            return x + y;
        }
        public override int Kurang(int x, int y)
        {
            return x - y;
        }
    }
}
