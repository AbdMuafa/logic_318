﻿//Konversi();
//operatorAritmatika();
//modulus();
//operatorPenugasan();
//operatorLogika();

//taskStatementElse();

//taskIsNested();
//kondisiTernary();
//manipulasiString();
//splitJoin();
subString();

static void subString()
{
    //contoh inputan P202305CX021
    Console.WriteLine("--Substring--");
    Console.Write("Masukkan kalimat = ");
    string kalimat = Console.ReadLine();

    Console.WriteLine($"String tahun = {kalimat.Substring(1, 4)}");
    Console.WriteLine($"String bulan = {kalimat.Substring(5, 2)}");
    Console.WriteLine($"Kode lokasi = {kalimat.Substring(7, 2)}");
    Console.WriteLine($"Nomor = {kalimat.Substring(9)}");
}

static void splitJoin()
{
    Console.WriteLine("--Split dan Join--");
    Console.Write("Masukkan kalimat = ");
    string kalimat = Console.ReadLine();

    string[] kataArray = kalimat.Split(' ');

    foreach (string item in kataArray)
    {
        Console.WriteLine(item);
        
        //menghitung length kalimat
        //Console.WriteLine(item.Length);
    }
        Console.WriteLine(string.Join(" + ", kataArray));
}

static void manipulasiString()
{
    Console.WriteLine("--Masipulasi String--");
    Console.Write("Masukkan kata : ");
    string kata = Console.ReadLine();
    string remove = kata;
    string insert = kata;
    string replace = kata;

    Console.WriteLine($"Panjang karakter dari {kata} = {kata.Length}");
    Console.WriteLine($"To Upper dari {kata} = {kata.ToUpper()}");
    Console.WriteLine($"To Lower dari {kata} = {kata.ToLower()}");
    Console.WriteLine($"Remove dari {remove} = {remove.Remove(11)}");
    Console.WriteLine($"Insert dari {insert} = {insert.Insert(5,"banget")}");
    Console.WriteLine($"Replace dari {replace} = {replace.Replace("id","net")}");
}

static void kondisiTernary()
{
    Console.WriteLine("--Ternary--");
    Console.Write("Masukkan nilai x = ");
    int nilaiX = int.Parse(Console.ReadLine());
    Console.Write("Masukkan nilai y = ");
    int nilaiY = int.Parse(Console.ReadLine());

    string hasilTernary = nilaiX > nilaiY ? "Nilai x lebih besar dari y" : "Nilai y lebih besar dari x";

    Console.Write(hasilTernary);
}

static void taskIsNested()
{
    int harga=0, kodeBaju = 0;
    string merkBaju="", ukuranBaju = "";
    Console.WriteLine("--Task If Nested--");
    Console.Write("Masukkan kode baju = ");
    kodeBaju = int.Parse(Console.ReadLine());
    Console.Write("Masukkan kode ukuran = ");
    ukuranBaju = Console.ReadLine();

    if (kodeBaju == 1)
    {
        merkBaju = "IMP";

        if (ukuranBaju == "s" || ukuranBaju == "S")
        {
            harga = 200000;
        }
        else if (ukuranBaju == "m" || ukuranBaju == "M")
        {
            harga = 220000;
        }
        else
        {
            harga = 250000;
        }
            //Console.WriteLine($"Merk Baju {merkBaju}\n Harga = {harga}");

    }
    else if (kodeBaju == 2)
    {
        merkBaju = "Prada";

        if (ukuranBaju == "s" || ukuranBaju == "S")
        {
            harga = 150000;
        }
        else if (ukuranBaju == "m" || ukuranBaju == "M")
        {
            harga = 160000;
        }
        else
        {
            harga = 170000;
        }
            //Console.WriteLine($"Merk Baju {merkBaju}\n Harga = {harga}");
    }
    else if (kodeBaju == 3)
    {
        merkBaju = "Gucci";

        if (ukuranBaju == "s" || ukuranBaju == "S")
        {
            harga = 200000;
        }
        else if (ukuranBaju == "m" || ukuranBaju == "M")
        {
            harga = 200000;
        }
        else
        {
            harga = 200000;
        }
    }
    else
    {
        Console.WriteLine("Inputan salah!!!");
    }
            Console.WriteLine($"Merk Baju {merkBaju}\nHarga = {harga}");

}

static void taskStatementElse()
{
    int mtk, fisika, kimia = 0;
    Console.WriteLine("--Task Statement Else--");
    Console.Write("Masukkan Nilai MTK = ");
    mtk = int.Parse(Console.ReadLine());

    Console.Write("Masukkan Nilai Fisika = ");
    fisika = int.Parse(Console.ReadLine());

    Console.Write("Masukkan Nilai Kimia = ");
    kimia = int.Parse(Console.ReadLine());

    int rataRata;

    rataRata = (mtk + fisika + kimia) / 3;

    Console.WriteLine("Nilai rata-rata = " + rataRata);
    Console.WriteLine("");

    if (rataRata > 50 && rataRata <= 100)
    {
        Console.WriteLine("Selamat");
        Console.WriteLine("Kamu Berhasil");
        Console.WriteLine("Kamu Hebat");
    }
    else if (rataRata >= 1 && rataRata <= 50)
    {
        Console.WriteLine("Maaf");
        Console.WriteLine("Kamu Gagal");
    }
    else
    {
        Console.WriteLine("Inputan Salah!!");
    }
}


static void operatorLogika()
{
    Console.WriteLine("--Operator Logika--");
    Console.Write("Masukkan umur = ");
    int umur = int.Parse(Console.ReadLine());
    Console.Write("Masukkan password = ");
    string password = Console.ReadLine();

    bool isAdult = umur > 18;
    bool isPasswordValid = password == "admin";

    if (isPasswordValid && isAdult)
    {
        Console.WriteLine("Anda sudah dewasa dan password valid");
    }
    else if (isAdult && !isPasswordValid)
    {
        Console.WriteLine("Anda sudah dewasa dan password invalid");
    }
    else if (!isAdult && isPasswordValid)
    {
        Console.WriteLine("Anda belum dewasa dan password valid");
    }
    else
    {
        Console.WriteLine("Anda belum dewasa dan password invalid");
    }
}

static void operatorPerbandingan()
{
    int mangga, apel = 0;

    Console.WriteLine("--Operator Perbandingan--");
    Console.Write("Jumlah mangga = ");
    mangga = int.Parse(Console.ReadLine());

    Console.Write("Jumlah apel = ");
    apel = int.Parse(Console.ReadLine());

    Console.WriteLine("Hasil Perbandingan : ");
    Console.WriteLine($"Mangga > Apel : {mangga > apel}");
    Console.WriteLine($"Mangga >= Apel : {mangga >= apel}");
    Console.WriteLine($"Mangga < Apel : {mangga < apel}");
    Console.WriteLine($"Mangga <= Apel : {mangga <= apel}");
    Console.WriteLine($"Mangga != Apel : {mangga != apel}");
    Console.WriteLine($"Mangga == Apel : {mangga == apel}");
}

static void operatorPenugasan()
{
    int mangga = 10;
    int apel = 8;

    // isi ulang nilai variabel
    mangga = 15;

    Console.WriteLine("--Operator Penugasan--");
    Console.WriteLine("Cetak mangga = " + mangga);

    apel += 6;

    Console.WriteLine("Cetak apel = " + apel);
}

static void modulus()
{
    int mangga, apel, hasil = 0;
    Console.WriteLine("--Operator Modulus--");
    Console.Write("Masukkan jumlah mangga = ");
    mangga = int.Parse(Console.ReadLine());

    Console.Write("Masukkan jumlah apel = ");
    apel = int.Parse(Console.ReadLine());

    hasil = mangga % apel;

    Console.WriteLine($"Hasil mangga % apel = {hasil}");
}

static void Konversi()
{
    int myInt = 10;
    double myDouble = 3.25;
    bool myBool = true;

    Console.WriteLine("Convert int to string : " + Convert.ToString(myInt));
    Console.WriteLine("Convert int to string : " + myInt.ToString());
    Console.WriteLine("Convert int to double : " + Convert.ToDouble(myInt));
    Console.WriteLine("Convert double to int : " + Convert.ToInt32(myDouble));
    Console.WriteLine("Convert bool to string : " + Convert.ToString(myBool));
}

static void operatorAritmatika()
{
    int mangga, apel, hasil = 0;
    Console.WriteLine("--Operator Aritmatika--");
    Console.Write("Masukkan jumlah mangga = ");
    mangga = int.Parse(Console.ReadLine());

    Console.Write("Masukkan jumlah apel = ");
    apel = int.Parse(Console.ReadLine());

    hasil = mangga + apel;

    Console.WriteLine($"Hasil mangga + apel = {hasil}");
}

Console.ReadKey();