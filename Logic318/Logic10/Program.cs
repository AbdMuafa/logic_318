﻿namespace Logic10
{
    public class Program
    {
        public Program()
        {
            menu();
        }
        static void Main(string[] args)
        {
            menu();
        }
        static void menu()
        {
            Console.WriteLine("==== Welcome to Day 10 ====");
            Console.WriteLine("|   1. Ice Cream Parlor      |");
            Console.WriteLine("|   2. Pairs                 |");
            Console.WriteLine("|   3. Cheess Engine         |");
            Console.WriteLine("|   4. Prima                 |");
            Console.WriteLine("|   5. Pajak                 |");
            Console.WriteLine("|   6. Gross Up Tax          |");
            Console.WriteLine("|   0. Exit		            |");
            Console.Write("Masukkan no soal: ");
            int soal = int.Parse(Console.ReadLine());

            switch (soal)
            {
                case 1:
                    IceCreamParlor soal1 = new IceCreamParlor();
                    kembali();
                    break;
                case 2:
                    Pairs soal2 = new Pairs();
                    kembali();
                    break;
                case 3:
                    CheessEngine soal3 = new CheessEngine();
                    kembali();
                    break;
                case 4:
                    Prima soal4 = new Prima();
                    kembali();
                    break;
                case 5:
                    Pajak soal5 = new Pajak();
                    kembali();
                    break;
                case 6:
                    GrossUpTax soal6 = new GrossUpTax();
                    kembali();
                    break;
                default:
                    break;
            }
            Console.Write("\nPress any key...");
            Console.ReadKey();
        }

        public static int Tambah(int a, int b)
        {
            return a + b;
        }

        static void kembali()
        {
            Console.WriteLine("\nApakah ingin kembali ke menu ? (y/n) ");
            string menuInput = Console.ReadLine().ToLower();
            if (menuInput == "y")
            {
                Console.Clear();
                menu();
            }
            else
            {
                Console.WriteLine("Terima kasih!");
                Console.WriteLine("Silahkan tekan Enter kembali...");
            }
        }

    }

}

