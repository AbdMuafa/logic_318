﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic10
{
    internal class Pajak
    {
        public Pajak()
        {
            Console.WriteLine("--Pajak--");
            Console.Write("Masukkan gaji : ");
            double gaji = double.Parse(Console.ReadLine());

            double pajak = 0, pajakPenghasilan = 0;

            if (gaji <= 25000)
            {
                pajak = 0;
                Console.WriteLine($"{gaji} = {pajak}");
            }
            else
            {
                pajakPenghasilan = gaji - 25000;
                if (pajakPenghasilan > 0)
                    if (pajakPenghasilan - 50000 > 0)
                        pajak += 50000 * 0.05;
                    else
                        pajak += pajakPenghasilan * 0.05;
                if (pajakPenghasilan > 50000)
                    if (pajakPenghasilan - 50000 > 50000)
                        pajak += 50000 * 0.1;
                    else
                        pajak += (pajakPenghasilan - 50000) * 0.1;
                if (pajakPenghasilan > 100000)
                    if (pajakPenghasilan - 100000 > 100000)
                        pajak += 100000 * 0.15;
                    else
                        pajak += (pajakPenghasilan - 100000) * 0.15;
                if (pajakPenghasilan > 200000)
                    pajak += (pajakPenghasilan - 200000) * 0.25;
            }
            Console.WriteLine();
            Console.WriteLine(pajak);
        }
    }
}
