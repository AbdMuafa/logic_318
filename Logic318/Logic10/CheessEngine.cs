﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility;

namespace Logic10
{
    internal class CheessEngine
    {
        public CheessEngine()
        {
            Console.Write("Queen White : ");
            string input = Console.ReadLine().ToUpper();

            Console.Write("Queen Black : ");
            string qBlack = Console.ReadLine().ToUpper();

            Console.Write("");

            string[,] cBoard = new string[4, 4];

            string rowMap = "4321";
            string colMap = "ABCD";

            for (int i = 0; i < cBoard.GetLength(0); i++)
            {
                for (int j = 0; j < cBoard.GetLength(1); j++)
                {
                    if (rowMap.IndexOf(input[1]) == i || colMap.IndexOf(input[0]) == j || rowMap.IndexOf(input[1]) + j == colMap.IndexOf(input[0]) + i || rowMap.IndexOf(input[1]) + colMap.IndexOf(input[0]) == i + j)
                    {
                        cBoard[i, j] = "x";
                    }
                    else
                    {
                        cBoard[i, j] = ".";
                    }

                }
            }

            if (cBoard[rowMap.IndexOf(qBlack[1]), colMap.IndexOf(qBlack[0])] == "x")
            {
                Console.WriteLine("YES");
            }
            else
            {
                Console.WriteLine("NO");
            }
            cBoard[rowMap.IndexOf(input[1]), colMap.IndexOf(input[0])] = "QW";
            cBoard[rowMap.IndexOf(qBlack[1]), colMap.IndexOf(qBlack[0])] = "QB";

            Printing.Array2Dim(cBoard);
        }
    }
}
