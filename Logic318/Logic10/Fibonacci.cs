﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic10
{
    internal class Fibonacci
    {
        public Fibonacci()
        {
            Console.WriteLine("--Fibonacci--");
            Console.Write("Masukkan jumlah bilangan : ");
            int fibon = int.Parse(Console.ReadLine());

            int angka1 = 0;
            int angka2 = 1;
            int angka3 = 1;

            for (int i = 0; i < fibon; i++)
            {
                angka3 = angka2 + angka1;
                angka1 = angka2;
                angka2 = angka3;

                Console.Write(angka1 + " , ");
            }
            Console.WriteLine();
        }
    }
}
