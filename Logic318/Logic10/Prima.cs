﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic10
{
    internal class Prima
    {
        public Prima()
        {
            Console.Write("Masukkan Batas Bilangan Prima : ");
            int bilangan = int.Parse(Console.ReadLine());

            //cara menggunakan while
            List<int> list = new List<int>();
            int angka = 2;
            while(list.Count < bilangan)
            {
                bool prime = true;
                for (int i = 2; i < angka; i++)
                {
                    if (angka % i == 0)
                    {
                        prime = false;
                        break;
                    }
                }
                if (prime)
                    list.Add(angka);
                angka++;
            }
            Console.Write($"{list}");
            //return list;

            //cara panggil method
            //for (int i = 2; i < bilangan+2; i++)
            //{
            //    if (FindPrime(i) == true)
            //    {
            //        Console.Write(i + " ");
            //    }
            //    else
            //    {
            //        bilangan++;
            //    }
            //}

            //bool prima = true;

            //if (bilangan >= 2)
            //{
            //    for (int i = 2; i <= bilangan; i++)
            //    {
            //        for (int j = 2; j < i; j++)
            //        {
            //            if ((i % j) == 0)
            //            {
            //                prima = false;
            //            }
            //        }

            //        if (prima)
            //            Console.Write($"{i} ");
            //        prima = true;
            //    }
            //}
        }

        //public static bool FindPrime(int angka)
        //{
        //    for (int j = 2; j < Math.Floor(Math.Sqrt(angka))+1; j++)
        //    {
        //        if (angka % j == 0) return false;                
        //    }
        //    return true;
        //}
    }
}
