﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic10
{
    internal class GrossUpTax
    {
        public GrossUpTax()
        {
            Console.WriteLine("--Gross Up Tax--");
            Console.Write("Masukkan gaji : ");
            int gaji = int.Parse(Console.ReadLine());

            int pajak = 0, freeTax = 25000;
            int pajakPenghasilan = gaji - freeTax;

            if (gaji <= 25000)
            {
                Console.WriteLine($"Income          = {gaji}");
                Console.WriteLine($"Tax Allowance   = -");
                Console.WriteLine($"Total Income    = {gaji}");
                Console.WriteLine($"Free Tax        = {freeTax}");
                Console.WriteLine($"Taxable-Income  = {gaji}");
                Console.WriteLine($"Tax Payable     = -");
            }
            else
            {
                if (pajakPenghasilan > 0)
                    if (pajakPenghasilan - 50000 > 0)
                        pajak += 50000 * 5 / 100;
                    else
                        pajak += pajakPenghasilan * 5 / 100;
                if (pajakPenghasilan > 50000)
                    if (pajakPenghasilan - 50000 > 50000)
                        pajak += 50000 * 5 / 100;
                    else
                        pajak += (pajakPenghasilan - 50000) * 10 / 100;
                if (pajakPenghasilan > 100000)
                    if (pajakPenghasilan - 100000 > 100000)
                        pajak += 100000 * 15 / 100;
                    else
                        pajak += (pajakPenghasilan - 100000) * 15 / 100;
                if (pajakPenghasilan > 200000)
                    pajak += (pajakPenghasilan - 200000) * 25 / 100;
            }
            Console.WriteLine();
            Console.WriteLine($"Income          = {gaji}");
            Console.WriteLine($"Tax Allowance   = -");
            Console.WriteLine($"Total Income    = {gaji}");
            Console.WriteLine($"Free Tax        = {freeTax}");
            Console.WriteLine($"Taxable-Income  = {gaji}");
            Console.WriteLine($"Tax Payable     = -");

            Console.WriteLine($"pajak : {pajak}");
            
        }

        //public static float hitung(int salary, int pajak)
        //{
        //    int slrAsli = salary;
        //    float pjk = 0;
        //    int sal = salary * pajak;
        //    if (sal > 25000)
        //    {
        //        sal -= 25000;
        //    }
        //}
    }
}
