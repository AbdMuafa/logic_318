﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic08
{
    internal class tesSort
    {
        public tesSort()
        {
            //string[] angka = new string("");
            Console.WriteLine("--Insertion Sort--");
            Console.Write("Masukkan jumlah angka : ");
            int jmlh = int.Parse(Console.ReadLine());
            for (int i = 0; i < jmlh; i++)
            {
                Console.Write("Masukkan urutan angka (kasih spasi) : ");
                string[] angka = Console.ReadLine().Split(' ');
                int[] randomArr = Array.ConvertAll<string, int>(angka, int.Parse);
                Utility.Printing.Array1Dim(Array.ConvertAll(randomArr, x => x.ToString()));
                Console.WriteLine();
                int[] sortedArr = Utility.Sorting.Insertion(randomArr);
                Utility.Printing.Array1Dim(Array.ConvertAll(sortedArr, x => x.ToString()));
            }
        }
    }
}
