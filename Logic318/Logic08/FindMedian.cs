﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility;

namespace Logic08
{
    internal class FindMedian
    {
        public FindMedian()
        {
            Console.Write("Angka : ");
            string[] angka = Console.ReadLine().Split(' ');
            int[] arr = Array.ConvertAll<string, int>(angka, int.Parse);

            arr = Sorting.Insertion(arr);

            foreach (int i in arr)
            {
                Console.Write(i);
            }
            Console.WriteLine();
            if (arr.Length % 2 == 0)
            {
                float n1 = arr[arr.Length / 2];
                float n2 = arr[arr.Length / 2 - 1];
                Console.Write($"{(n1+n2)/2}");
            }
            else
            {
                Console.WriteLine($"{arr[arr.Length / 2]}");
            }
        }
    }
}
