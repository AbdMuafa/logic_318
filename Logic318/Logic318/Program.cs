﻿// See https://aka.ms/new-console-template for more information

//output
Console.WriteLine("Hello, World!");

//input implisit
Console.Write("Masukkan Nama Anda : ");
string nama = Console.ReadLine();

//output pertama
//Console.WriteLine("Nama anda : " + nama);

//output kedua
Console.WriteLine("Nama anda : {0}", nama);

//output ketiga
Console.WriteLine($"Nama anda : {nama}");

// eksplisit
var namaVariable = "value namaVariable" ;

Console.ReadKey();