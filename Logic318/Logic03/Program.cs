﻿//Break();
//Continue();

//indexOf();
//dateTimeParsing();
//dateTimeProperties();
timeSpan();

Console.ReadKey();

static void timeSpan()
{
    Console.WriteLine("--Time Span--");

    DateTime date1 = new DateTime(2023, 5, 4, 10, 35, 35);
    DateTime date2 = new DateTime(2023, 7, 14, 12, 45, 45);

    TimeSpan interval = date2 - date1;

    Console.WriteLine("Nomor of days : " + interval.Days);
    Console.WriteLine("Total Nomor of days : " + interval.TotalDays);
    Console.WriteLine("Nomor of hours : " + interval.Hours);
    Console.WriteLine("Total Nomor of hours : " + interval.TotalHours);
    Console.WriteLine("Nomor of minutes : " + interval.Minutes);
    Console.WriteLine("Total Nomor of minutes : " + interval.TotalMinutes);
    Console.WriteLine("Nomor of seconds : " + interval.Seconds);
    Console.WriteLine("Total Nomor of seconds : " + interval.TotalSeconds);
}

static void dateTimeProperties()
{
    Console.WriteLine("--Penggunaan Datetime Properties--");
    DateTime date = new DateTime(2023, 5, 4, 11, 1, 45);
    int tahun = date.Year;
    int bulan = date.Month;
    int hari = date.Day;
    int jam = date.Hour;
    int menit = date.Minute;
    int detik = date.Second;
    int weekDay = (int)date.DayOfWeek;
    string hariString = date.ToString("dddd");
    string hariString2 = date.DayOfWeek.ToString();

    Console.WriteLine($"Tahun : {tahun}");
    Console.WriteLine($"Bulan : {bulan}");
    Console.WriteLine($"Hari : {hari}");
    Console.WriteLine($"Jam : {jam}");
    Console.WriteLine($"Menit : {menit}");
    Console.WriteLine($"Detik : {detik}");
    Console.WriteLine($"Week Day : {weekDay}");
    Console.WriteLine($"Hari ini : {hariString}");
    Console.WriteLine($"Hari ini (cara kedua) : {hariString2}");
}

static void dateTimeParsing()
{
    Console.WriteLine("--Penggunaan Datetime Parsing--");
    Console.Write("Masukkan date time (tanggal/bulan/tahun) : ");
    string dateString = Console.ReadLine();

    try
    {
        DateTime date = DateTime.ParseExact(dateString, "dd/MM/yyyy", null);

        Console.WriteLine(date);
    }
    catch (Exception e)
    {
        Console.WriteLine("Masukkan format yang benar : (dd/MM/yyyy)");
        Console.WriteLine("Pesan error : " + e.Message);
    }

}

static void dateTime()
{
    Console.WriteLine("--Penggunaan Datetime--");
    DateTime dt1 = new DateTime();
    Console.WriteLine(dt1);

    DateTime dt2 = DateTime.Now;
    Console.WriteLine(dt2);

    DateTime dt3 = new DateTime(2023, 5, 4);
    Console.WriteLine(dt3);

    DateTime dt4 = new DateTime(2023, 12, 22, 11, 55, 45);
    Console.WriteLine(dt4);
}

static void indexOf()
{
    List<int> list = new List<int>()
    {
        1, 2, 3, 4, 5, 6, 7
    };
    int[] numArr = new int[] { 6, 7, 8, 9, 10, 11, 12, 13 };

    Console.WriteLine("Masukkan item (value) : ");
    int item = int.Parse(Console.ReadLine());

    int indexList = list.IndexOf(item);
    int indexArray = Array.IndexOf(numArr, item);
    if (indexList != -1)
    {
        Console.WriteLine("List element {0} is found at index {1}", item, indexList);
    }
    else
    {
        Console.WriteLine("Element not found in the given List");
    }

    if (indexArray != -1)
    {
        Console.WriteLine("Array element {0} is found at index {1}", item, indexArray);
    }
    else
    {
        Console.WriteLine("Element not found in the given Array");
    }

}

static void stringToCharArray()
{
    Console.WriteLine("--String To Char Array--");
    Console.Write("Masukkan kalimat : ");
    string kalimat = Console.ReadLine();

    char[] array = kalimat.ToCharArray();

    for (int i = 0; i < array.Length; i++)
    {
        Console.WriteLine(array[i]);
    }
}

static void Break()
{
    Console.WriteLine("--Mencoba perulangan break");
    Console.Write("Masukkan jumlah angka perulangan : ");
    int angka = int.Parse(Console.ReadLine());
    Console.Write("Masukkan angka yang ingin di Break / berhenti : ");
    int henti = int.Parse(Console.ReadLine());
    for (int i = 0; i < angka; i++)
    {
        if (i == henti)
        {
            break;
        }
        Console.WriteLine(i);
    }
}

static void Continue()
{
    Console.WriteLine("--Mencoba perulangan continue--");
    Console.Write("Masukkan jumlah angka perulangan : ");
    int angka = int.Parse(Console.ReadLine());
    Console.Write("Masukkan angka yang ingin di Continue / Lewati: ");
    int lewati = int.Parse(Console.ReadLine());
    for (int i = 0; i < angka; i++)
    {
        if (i == lewati)
        {
            continue;
        }
        Console.WriteLine(i);
    }
}