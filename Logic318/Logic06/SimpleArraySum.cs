﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic06
{
    internal class SimpleArraySum
    {
        public SimpleArraySum()
        {
            //Console.Write("Masukkan beberapa angka : ");
            //string[] splitStr = Console.ReadLine().Split(' ');

            //int[] splitInt = Array.ConvertAll<string, int>(splitStr,int.Parse);
            //for (int i = 0; i < splitInt.Length; i++)
            //{
            //    Console.WriteLine(splitInt[i] + 1);
            //}

            Console.WriteLine("Masukkan angka : ");
            string[] array = Console.ReadLine().Split(' ');
            int[] arr = Array.ConvertAll<string, int>(array, int.Parse);

            int sum = 0;
            for (int i = 0; i < arr.Length; i++)
            {
                sum += arr[i];
            }
            Console.WriteLine(sum);
        }
    }
}
