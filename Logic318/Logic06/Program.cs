﻿namespace Logic06
{
	public class Program
	{
		public Program()
		{
			menu();
		}
		static void Main(string[] args)
		{
			menu();
		}
		static void menu()
		{
			Console.WriteLine("==== Welcome to Day 06 ====");
			Console.WriteLine("|   1. Solve Me First		|");
			Console.WriteLine("|   2. Time Conversion		|");
			Console.WriteLine("|   3. Simple Array		|");
			Console.WriteLine("|   4. Diagonal			|");
			Console.WriteLine("|   5. Stair Case		|");
			Console.WriteLine("|   6. Dimension Case Study	|");
			Console.WriteLine("|   7. Birthday Candle		|");
			Console.WriteLine("|   8. Very Big Sum		|");
			Console.WriteLine("|   9. Compare The Triplets	|");
			Console.WriteLine("|   10. Camel Case	|");
			Console.WriteLine("|   0. Exit			|");
			Console.Write("Masukkan no soal: ");
			int soal = int.Parse(Console.ReadLine());

			switch (soal)
			{
				case 1:
					SolveMeFirst soal01 = new SolveMeFirst();
					kembali();
					break;
				case 2:
					TimeConversion soal02 = new TimeConversion();
					kembali();
					break;
				case 3:
					SimpleArraySum soal03 = new SimpleArraySum();
					kembali();
					break;
				case 4:
					Diagonal soal04 = new Diagonal();
					kembali();
					break;
				case 5:
					stairCase soal05 = new stairCase();
					kembali();
					break;
				case 6:
					dimensionCaseStudy soal06 = new dimensionCaseStudy();
					kembali();
					break;
				case 7:
					cakeCandle soal07 = new cakeCandle();
					kembali();
					break;
				case 8:
					veryBigSum soal08 = new veryBigSum();
					kembali();
					break;
				case 9:
					compareTheTriplets soal09 = new compareTheTriplets();
					kembali();
					break;
				default:
					break;
			}
			//Console.Write("\nPress any key...");
			//Console.ReadKey();
		}

		public static int Tambah(int a, int b)
		{
			return a + b;
		}

		static void kembali()
		{
			Console.WriteLine("\nApakah ingin kembali ke menu ? (y/n) ");
			string menuInput = Console.ReadLine().ToLower();
			if (menuInput == "y")
			{
				Console.Clear();
				menu();
			}
			else
			{
				Console.WriteLine("Terima kasih!");
				Console.WriteLine("Silahkan tekan Enter kembali...");
			}
		}

	}

}

