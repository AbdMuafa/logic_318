﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic06
{
    internal class stairCase
    {
        public stairCase()
        {
            Console.WriteLine();
            Console.Write("Masukkan panjang segitiga : ");
            int panjang = int.Parse(Console.ReadLine());

            for (int i = 0; i < panjang; i++)
            {
                Console.Write("\t");
                for (int j = 0; j < panjang; j++)
                {
                    // dibawah ini untuk membuat segitiga siku sisi kanan dengan isi full
                    //if (j < panjang - 1 - i)
                    //{
                    //    Console.Write(" ");
                    //}
                    //else
                    //{
                    //    Console.Write(" ");
                    //}
                    // dibawah ini untuk membuat segitiga siku sisi kanan dengan tidak ada isinya
                    if (i + j == panjang -1 || i == panjang - 1 || j == panjang - 1)
                    {
                        Console.Write("#");
                    }
                    else
                    {
                        Console.Write(" ");
                    }
                }
                Console.WriteLine();
            }
        }
    }
}
