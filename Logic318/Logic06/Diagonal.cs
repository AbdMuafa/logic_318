﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility;

namespace Logic06
{
    internal class Diagonal
    {
        public Diagonal()
        {
            int brs = 5;
            int kol = 5;
            Console.WriteLine("--Diagonal--");
            string[,] data = new string[brs, kol];

            for (int row = 0; row < brs; row++)
            {
                for (int col = 0; col < kol; col++)
                {
                    if (row == col)
                    {
                        data[row, col] = "x";
                    }

                    else if (row + col == kol - 1)
                    {
                        //data[row, col] = "y";
                        data[row, col] = "x";
                    }
                }
            }
            Printing.Array2Dim(data);
        }
    }
}
