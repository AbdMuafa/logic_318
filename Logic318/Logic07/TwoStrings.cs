﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic07
{
    internal class TwoStrings
    {
        public TwoStrings()
        {
            Console.WriteLine("--Two Strings--");
            //Console.Write("Masukkan jumlah data : ");
            //int jmlh = int.Parse(Console.ReadLine());

            ////string[] gemstone = new string[jmlh];
            //string gemstone = "";

            //int[] alfabet = new int[26];
            //for (int i = 1; i <= jmlh; i++)
            //{
            //    Console.Write($"Data ke-{i} : ");
            //    gemstone = Console.ReadLine();
            //    //menghapus duplikat huruf perbaris input data
            //    char[] uniqueCharArray = gemstone.ToCharArray().Distinct().ToArray();
            //    var resultString = new string(uniqueCharArray);

            //    for (int j = 0; j < resultString.Length; j++)
            //    {
            //        //pengurangan di char
            //        alfabet[resultString[j] - 'a']++;
            //    }
            //}

            Console.Write("Masukkan data 1 : ");
            string data1 = Console.ReadLine();
            Console.Write("Masukkan data 2 : ");
            string data2 = Console.ReadLine();

            bool isToString = false;
            for (int i = 0; i < data1.Length; i++)
            {
                for (int j = 0; j < data2.Length; j++)
                {
                    if (data1[i] == data2[j])
                    {
                        isToString = true;
                        break;
                    }
                }
            }
            Console.WriteLine(isToString ? "YES" : "NO");

        }
    }
}
