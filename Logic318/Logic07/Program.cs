﻿namespace Logic07
{
    public class Program
    {
        public Program()
        {
            menu();
        }
        static void Main(string[] args)
        {
            menu();
        }
        static void menu()
        {
            Console.WriteLine("==== Welcome to Day 07 ====");
            Console.WriteLine("|   1. Camel Case	|");
            Console.WriteLine("|   2. Strong Password	|");
            Console.WriteLine("|   3. Caesar Chipper	|");
            Console.WriteLine("|   4. Mars Exploration	|");
            Console.WriteLine("|   5. HackerRank String|");
            Console.WriteLine("|   6. Pangram		|");
            Console.WriteLine("|   8. Gemstone		|");
            Console.WriteLine("|   9. Making Anagrams  |");
            Console.WriteLine("|   10. Two Strings     |");
            Console.WriteLine("|   0. Exit		|");
            Console.Write("Masukkan no soal: ");
            int soal = int.Parse(Console.ReadLine());

            switch (soal)
            {
                case 1:
                    camelCase soal1 = new camelCase();
                    kembali();
                    break;
                case 2:
                    StrongPassword soal2 = new StrongPassword();
                    kembali();
                    break;
                case 3:
                    CaesarChipper soal3 = new CaesarChipper();
                    kembali();
                    break;
                case 4:
                    MarsExploration soal4 = new MarsExploration();
                    kembali();
                    break;
                case 5:
                    HackerRankString soal5 = new HackerRankString();
                    kembali();
                    break;
                case 6:
                    Pangram soal6 = new Pangram();
                    kembali();
                    break;
                case 7:
                    SeparateNumbers soal7 = new SeparateNumbers();
                    kembali();
                    break;
                case 8:
                    Gemstone soal8 = new Gemstone();
                    kembali();
                    break;
                case 9:
                    MakingAnagrams soal9 = new MakingAnagrams();
                    kembali();
                    break;
                case 10:
                    TwoStrings soal10 = new TwoStrings();
                    kembali();
                    break;
                default:
                    break;
            }
            //Console.Write("\nPress any key...");
            //Console.ReadKey();
        }

        public static int Tambah(int a, int b)
        {
            return a + b;
        }

        static void kembali()
        {
            Console.WriteLine("\nApakah ingin kembali ke menu ? (y/n) ");
            string menuInput = Console.ReadLine().ToLower();
            if (menuInput == "y")
            {
                Console.Clear();
                menu();
            }
            else
            {
                Console.WriteLine("Terima kasih!");
                Console.WriteLine("Silahkan tekan Enter kembali...");
            }
        }

    }

}

