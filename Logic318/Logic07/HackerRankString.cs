﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic07
{
    internal class HackerRankString
    {
        public HackerRankString()
        {
            Console.Write("Masukkan kalimat : ");
            string kalimat = Console.ReadLine().ToLower();

            Console.Write("Masukkan kalimat yang dicari : ");
            string alfabet = Console.ReadLine().ToLower();
            //string alfabet = "hackerrank";
            int index = 0;

            for (int i = 0; i < kalimat.Length; i++)
            {
                if (index< alfabet.Length && kalimat[i] == alfabet[index])
                {
                    index++;
                }
            }
            //if (index == alfabet.Length)
            //{
            //    Console.WriteLine("NO");
            //}else
            //{
            //    Console.WriteLine("YES");
            //}

            Console.WriteLine(index);
            Console.WriteLine(index == alfabet.Length ? "YES" : "NO");
        }
    }
}
