﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic07
{
    internal class MakingAnagrams
    {
        public MakingAnagrams()
        {
            Console.WriteLine("--Making Anagrams--");
            int count = 0;
            //int[] alfabet = new int[26];

            Console.Write("Masukkan data 1 : ");
            string data1 = Console.ReadLine();
            Console.Write("Masukkan data 2 : ");
            string data2 = Console.ReadLine();

            int panjangK1 = data1.Length;
            int panjangK2 = data2.Length;

            List<char> ListK1 = new List<char>(panjangK1);
            List<char> ListK2 = new List<char>(panjangK2);

            ListK1 = data1.ToList();
            ListK2 = data2.ToList();

            int total = panjangK1 + panjangK2;

            for (int i = 0; i < panjangK1; i++)
            {
                if (ListK2.Contains(ListK1[i]))
                {
                    count++;
                    ListK2.Remove(ListK1[i]);
                }
            }
            Console.WriteLine($"Jumlah huruf yang berbeda adalah {total - (2 * count)}");

            //int panjangA = data1.Length;
            //int panjangB = data2.Length;
            //int total = panjangA + panjangB;

            //for (int i = 0; i < data1.Length; i++)
            //{
            //    for (int j = 0; j < data2.Length; j++)
            //    {
            //        if (data1[i] == data2[j])
            //        {
            //            count++;
            //        }
            //    }
            //}
            //Console.WriteLine(total - count);

        }
    }
}
