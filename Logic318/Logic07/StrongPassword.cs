﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic07
{
    internal class StrongPassword
    {
        public StrongPassword()
        {
            Console.WriteLine("--Strong Password--");
            Console.Write("Password : ");
            string password = Console.ReadLine();

            string libNumber = "1234567890";
            string lower_case = "abcdefghijklmnopqrstuvwxyz";
            string upper_case = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            string special_characters = "!@#$%^&*()-+";

            int charToAdded = 0;

            if (password.IndexOfAny(libNumber.ToCharArray()) == -1)
            {
                charToAdded += 1;
            }
            if (password.IndexOfAny(lower_case.ToCharArray()) == -1)
            {
                charToAdded += 1;
            }
            if (password.IndexOfAny(upper_case.ToCharArray()) == -1)
            {
                charToAdded += 1;
            }
            if (password.IndexOfAny(special_characters.ToCharArray()) == -1)
            {
                charToAdded += 1;
            }

            if (password.Length < 6)
            {
                charToAdded = charToAdded > 6 - password.Length ? charToAdded : 6 - password.Length;
            }

            Console.WriteLine(charToAdded);
        }
    }
}
