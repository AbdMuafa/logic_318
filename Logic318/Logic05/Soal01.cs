﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility;

namespace Logic05
{
	public class Soal01
	{
		public Soal01()
		{
			// 0	1	2	3	4	5
			// 1	3	5	7	11	13
			Console.Write("Masukkan N: ");
			int jumlah = int.Parse(Console.ReadLine());
			string[] arrString = new string[jumlah];
			for (int i = 0; i < jumlah; i++)
			{
				//Console.Write((1+2*i) + "\t");
				arrString[i] = (i * 2 + 1).ToString();
			}
			Printing.Array1Dim(arrString);
		}
	}
}
