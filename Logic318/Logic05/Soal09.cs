﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic05
{
	internal class Soal09
	{
		public Soal09()
		{
			//4	16	*	64	256	*	1024
			Console.Write("Masukkan N: ");
			int jumlah = int.Parse(Console.ReadLine()); ;
			int angka = 4;
			for (int i = 1; i <= jumlah; i++)
			{
				if (i % 3 == 0)
					Console.Write("* \t");
				else
				{
					Console.Write(angka + "\t");
					angka *= 4;
				}
			}
		}
	}
}
