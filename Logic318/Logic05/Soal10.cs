﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic05
{
	internal class Soal10
	{
		public Soal10()
		{
			//3	9	27	XXX	243	729	2187
			Console.Write("Masukkan N: ");
			int jumlah = int.Parse(Console.ReadLine()); ;
			for (int i = 1; i <= jumlah; i++)
			{
				Console.Write(i != 4 ? $"{Math.Pow(3, i)}\t" : $"{digitToString(Math.Pow(3, i))}\t");
			}


		}

		private string digitToString(double digit)
		{
			string result = "";
			for (int i = 0; i < digit.ToString().Length; i++)
			{
				result += "X";
			}


			return result;

		}
	}
}
