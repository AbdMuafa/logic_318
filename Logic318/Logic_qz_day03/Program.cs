﻿menu();
Console.ReadKey();

static void menu()
{
    bool ulangi = true;
    int input;
    while (ulangi)
    {
        Console.WriteLine("\n----Menu tugas Logic 03----");
        Console.WriteLine("|   1. Grade Nilai         |");
        Console.WriteLine("|   2. Pulsa               |");
        Console.WriteLine("|   3. Grab Food           |");
        Console.WriteLine("|   4. Puntung Rokok       |");
        Console.WriteLine("|   5. Gen                 |");
        Console.WriteLine("|   6. Perhitungan Gaji    |");

        Console.Write("Pilih menu : ");
        input = int.Parse(Console.ReadLine());

        switch (input)
        {
            case 1:
                gradeNilai();
                break;
            case 2:
                pulsa();
                break;
            case 3:
                grabFood();
                break;

            case 5:
                gen();
                break;
            case 6:
                perhitunganGaji();
                break;

                break;
            default:
                Console.WriteLine("Masukkan hanya 1 - 6 dan angka!");
                break;
        }
        ulangi = false;
    }
}

static void kembali()
{
    Console.WriteLine("\nApakah ingin kembali ke menu ? (y/n) ");
    string menuInput = Console.ReadLine().ToLower();
    if (menuInput == "y")
    {
        Console.Clear();
        menu();
    }
    else
    {
        Console.WriteLine("Terima kasih!");
        Console.WriteLine("Silahkan tekan Enter kembali...");
    }
}

static void gradeNilai()
{
    Console.WriteLine("\n--Grade Nilai Mahasiswa--");
    Console.Write("Masukkan nilai mahasiswa = ");
    string input = Console.ReadLine().Replace(" ", "");
    string grade = "";

    if (input == "")
    {
        Console.WriteLine("HARAP MASUKKAN NILAI JANGAN KOSONG!");
    }
    else
    {
        //convert string to int
        int nilai = int.Parse(input);
        if (nilai >= 0)
        {
            if (nilai >= 90 && nilai <= 100)
            {
                grade = "A";
            }
            else if (nilai >= 70 && nilai <= 89)
            {
                grade = "B";
            }
            else if (nilai >= 50 && nilai <= 69)
            {
                grade = "C";
            }
            else if (nilai >= 0 && nilai < 50)
            {
                grade = "E";
            }
            Console.WriteLine($"Grade : {grade}");
        }
        else
        {
            Console.WriteLine("Input yang anda masukkan salah!");
        }
    }



    kembali();
}

static void pulsa()
{
    Console.WriteLine("\n--Membeli Pulsa--");
    Console.WriteLine("Keterangan : \nPulsa 10.000 mendapatkan poin 80 \nPulsa 20.000 mendapatkan poin 80");
    Console.WriteLine("Pulsa 25.000 mendapatkan poin 200 \nPulsa 50.000 mendapatkan poin 400 \nPulsa 100.000 mendapatkan poin 800");
    int pulsa, poin, hasil;
    Console.Write("Masukkan jumlah pulsa : ");
    pulsa = int.Parse(Console.ReadLine());

    if (pulsa == 100000)
    {
        Console.WriteLine($"Pulsa : {pulsa}");
        Console.WriteLine("Point : 800");
    }
    else if (pulsa >= 50000 && pulsa < 100000)
    {
        Console.WriteLine($"Pulsa : {pulsa}");
        Console.WriteLine("Point : 400");
    }
    else if (pulsa >= 25000 && pulsa < 50000)
    {
        Console.WriteLine($"Pulsa : {pulsa}");
        Console.WriteLine("Point : 200");
    }
    else if (pulsa >= 10000 && pulsa < 25000)
    {
        Console.WriteLine($"Pulsa : {pulsa}");
        Console.WriteLine("Point : 80");
    }
    else
    {
        Console.WriteLine("Inputan yang anda masukkan tidak benar!");
    }

    kembali();
}

static void grabFood()
{
    Console.WriteLine("\n--GrabFood--");

    int jarak = 0, belanja = 0, diskon = 0, total = 0, ongkir = 0;
    string promo = "";
    Console.Write("Belanja : ");
    belanja = int.Parse(Console.ReadLine());
    Console.Write("Jarak : ");
    jarak = int.Parse(Console.ReadLine());
    Console.Write("Masukkan Promo = ");
    promo = Console.ReadLine().ToUpper();
    bool isPromo = promo == "JKTOVO";

    if (jarak > 5)
    {

        if (isPromo == true)
        {
            if (belanja >= 30000)
            {
                diskon = belanja * 40 / 100;

            }

        }
    }
    else
    {

    }


    Console.WriteLine($"Belanja       : {belanja}");
    Console.WriteLine($"Diskon Ongkir : {diskon}");
    Console.WriteLine($"Ongkos Kirim  : {ongkir}");
    Console.WriteLine($"Total Belanja : {total}");


    kembali();
}

static void gen()
{
    string nama = "";
    string gen = "";
    int tahun = 0;
    Console.WriteLine("--Gen--");
    Console.Write("Masukkan nama anda : ");
    nama = Console.ReadLine();
    Console.Write("Tahun berapa anda lahir? ");
    tahun = int.Parse(Console.ReadLine());

    if (tahun >= 1995 && tahun <= 2015)
    {
        gen = "Z";
    }
    else if (tahun >= 1980 && tahun <= 1994)
    {
        gen = "Y (Millenials)";
    }
    else if (tahun >= 1979 && tahun <= 1965)
    {
        gen = "X";
    }
    else if (tahun >= 1964 && tahun <= 1944)
    {
        gen = "Baby Boomer";
    }
    else
    {
        Console.WriteLine("Inputan yang anda masukkan salah!");
    }

    Console.WriteLine($"{nama}, berdasarkan tahun lahir anda tergolong Generasi {gen}");
}

static void perhitunganGaji()
{
    Console.WriteLine("\n--Perhitungan Gaji--");
    Console.WriteLine("Input : ");
    Console.Write("Nama : ");
    string nama = Console.ReadLine();
    Console.Write("Tunjangan : Rp.");
    int tunjangan = int.Parse(Console.ReadLine());
    Console.Write("Gaji Pokok : ");
    int gapok = int.Parse(Console.ReadLine());
    Console.Write("Banyak bulan kerja : ");
    int bulan = int.Parse(Console.ReadLine());

    int pajak = 0, bpjs = 0, gajiPerbulan = 0;
    Int32 totalGaji = 0, gatun = 0;

    gatun = gapok + tunjangan;

    if (gatun > 10000000)
    {
        pajak = gatun * 15 / 100;
        bpjs = gatun * 3 / 100;
        gajiPerbulan = gatun - (pajak + bpjs);
        totalGaji = gatun - (pajak + bpjs) * bulan;
    }
    else if (gatun > 5000000 && gatun <= 10000000)
    {
        pajak = gatun * 10 / 100;
        bpjs = gatun * 3 / 100;
        gajiPerbulan = gatun - (pajak + bpjs);
        totalGaji = gatun - (pajak + bpjs) * bulan;
    }
    else if (gatun > 100000 && gatun <= 5000000)
    {
        pajak = gatun * 5 / 100;
        bpjs = gatun * 3 / 100;
        gajiPerbulan = gatun - (pajak + bpjs);
        totalGaji = gatun - (pajak + bpjs) * bulan;
    }
    else
    {
        Console.WriteLine("Inputan anda tidak sesuai!");
    }

    Console.WriteLine($"Karyawan atas nama {nama} slip gaji sebagai berikut: ");
    Console.WriteLine($"Pajak : Rp. {pajak} ");
    Console.WriteLine($"BPJS : Rp. {bpjs} ");
    Console.WriteLine($"Gaji/bulan : Rp. {gajiPerbulan} ");
    Console.WriteLine($"Total gaji : Rp. {totalGaji} ");


}