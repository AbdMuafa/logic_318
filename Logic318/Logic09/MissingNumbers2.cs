﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic09
{
    internal class MissingNumbers2
    {
        public MissingNumbers2()
        {
            Console.Write("input : ");
            //int[] arr = { 1, 3, 7, 5, 6, 2 };
            string[] angka = Console.ReadLine().Split(' ');
            int[] arr = Array.ConvertAll<string, int>(angka, int.Parse);
            int n = arr.Length;

            int[] temp = new int[n + 1];

            for (int i = 0; i < n; i++)
            {
                temp[arr[i] - 1] = 1;
            }

            int ans = 0;
            for (int i = 0; i <= n; i++)
            {
                if (temp[i] == 0)
                    ans = i + 1;
            }
            Console.WriteLine(ans);
        }
    }
}
