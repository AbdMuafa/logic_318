﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic09
{
    internal class DateTimeReview
    {
        public DateTimeReview()
        {
            Console.WriteLine("--DateTime Review--");
            Console.WriteLine("Cara input : yyyy-MM-dd");
            Console.Write("Tanggal awal : ");
            string[] dtAwal = Console.ReadLine().Split('T');
            string[] dateAwal = dtAwal[0].Split('-');
            string[] timeAwal = dtAwal[1].Split(':');
            DateTime awal = new DateTime(int.Parse(dateAwal[0]), int.Parse(dateAwal[1]), int.Parse(dateAwal[2]), int.Parse(timeAwal[0]), int.Parse(timeAwal[1]), int.Parse(timeAwal[2]));

            Console.Write("Tanggal akhir : ");
            string[] dtAkhir = Console.ReadLine().Split('T');
            string[] dateAkhir = dtAkhir[0].Split('-');
            string[] timeAkhir = dtAkhir[1].Split(':');
            DateTime akhir = new DateTime(int.Parse(dateAkhir[0]), int.Parse(dateAkhir[1]), int.Parse(dateAkhir[2]), int.Parse(timeAkhir[0]), int.Parse(timeAkhir[1]), int.Parse(timeAkhir[2]));

            //TimeSpan diff = DateTime.Now - dateTimeAkhir;
            TimeSpan diff = akhir - awal;

            Console.WriteLine($"Selisih hari {awal.ToString("yyyy-MM-ddThh:mm:ss")} - {akhir.ToString("yyyy-MM-ddThh:mm:ss")}");
            Console.WriteLine($"Selisih hari : {diff.Days}");
            Console.WriteLine($"Selisih jam : {diff.Hours}");
            Console.WriteLine($"Selisih menit : {diff.Minutes}");
            Console.WriteLine($"Selisih detik : {diff.Seconds}");
        }
    }
}
