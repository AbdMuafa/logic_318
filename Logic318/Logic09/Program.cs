﻿namespace Logic09
{
    public class Program
    {
        public Program()
        {
            menu();
        }
        static void Main(string[] args)
        {
            menu();
        }
        static void menu()
        {
            Console.WriteLine("==== Welcome to Day 09 ====");
            Console.WriteLine("|   1. Missing Numbers       |");
            Console.WriteLine("|   2. Sherlock and Array    |");
            Console.WriteLine("|   3. Date Time Review      |");
            Console.WriteLine("|   3. Date Time Test        |");
            Console.WriteLine("|   0. Exit		|");
            Console.Write("Masukkan no soal: ");
            int soal = int.Parse(Console.ReadLine());

            switch (soal)
            {
                case 1:
                    MissingNumbers soal1 = new MissingNumbers();
                    kembali();
                    break;
                case 2:
                    SherlockandArray soal2 = new SherlockandArray();
                    kembali();
                    break;
                case 3:
                    DateTimeReview soal3 = new DateTimeReview();
                    kembali();
                    break;
                case 4:
                    dateTimeTest soal4 = new dateTimeTest();
                    kembali();
                    break;
                default:
                    break;
            }
            Console.Write("\nPress any key...");
            Console.ReadKey();
        }

        public static int Tambah(int a, int b)
        {
            return a + b;
        }

        static void kembali()
        {
            Console.WriteLine("\nApakah ingin kembali ke menu ? (y/n) ");
            string menuInput = Console.ReadLine().ToLower();
            if (menuInput == "y")
            {
                Console.Clear();
                menu();
            }
            else
            {
                Console.WriteLine("Terima kasih!");
                Console.WriteLine("Silahkan tekan Enter kembali...");
            }
        }

    }

}

