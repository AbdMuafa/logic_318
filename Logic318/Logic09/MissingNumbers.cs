﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic09
{
    internal class MissingNumbers
    {
        public MissingNumbers()
        {

            Console.Write("Tamu : ");
            List<int> listTamu = Console.ReadLine().Split(' ').Select(int.Parse).ToList();
            Console.Write("Daftar : ");
            List<int> listDaftar = Console.ReadLine().Split(' ').Select(int.Parse).ToList();

            for (int i = 0; i < listTamu.Count; i++)
            {
                for (int j = 0; j < listDaftar.Count; j++)
                {
                    if (listTamu[i] == listDaftar[j])
                    {
                        listTamu.RemoveAt(i);
                        listDaftar.RemoveAt(j);
                        i--; j--;
                        break;
                    }
                }
            }
            listDaftar.Sort();
            Console.WriteLine($"Sisa : {String.Join(" ", listDaftar.Distinct())}");

            //inputan dinamis masih gagal
            //Console.Write("Masukkan jumlah data 1 : ");
            //int jmlh = int.Parse(Console.ReadLine());

            //List<int> listTamu = new List<int>(0);

            ////int[] alfabet = new int[26];
            //for (int i = 1; i <= jmlh; i++)
            //{
            //    Console.Write($"Data 1 ke-{i} : ");
            //    listTamu = Console.ReadLine().Split(' ').Select(int.Parse).ToList();

            //}

            //Console.Write("Masukkan jumlah data 2 : ");
            //int jmlh2 = int.Parse(Console.ReadLine());

            //List<int> listDaftar = new List<int>(0);

            //for (int i = 1; i <= jmlh2; i++)
            //{
            //    Console.Write($"Data 2 ke-{i} : ");
            //    listDaftar = Console.ReadLine().Split(' ').Select(int.Parse).ToList();
            //}

            //int panjangK1 = number1.Length;
            //int panjangK2 = listDaftar.Length;

            //List<char> ListK1 = new List<char>(panjangK1);
            //List<char> ListK2 = new List<char>(panjangK2);
            //List<char> list3 = new List<char>();

            //ListK1 = number1.ToList();
            //ListK2 = listDaftar.ToList();

            //for (int i = 0; i < ListK1.Count; i++)
            //{
            //    if (!ListK2.Contains(ListK1[i]))
            //        list3.Add(ListK1[i]);
            //    if (ListK2.Contains(ListK1[i]))
            //        ListK2.Remove(ListK1[i]);
            //}

            //for (int i = 0; i < ListK2.Count; i++)
            //{
            //    if (!ListK1.Contains(ListK2[i]))
            //        list3.Add(ListK2[i]);
            //    if (ListK1.Contains(ListK2[i]))
            //        ListK1.Remove(ListK2[i]);
            //}

            //Console.WriteLine();

            //for (int i = 0; i < list3.Count; i++)
            //{
            //    Console.WriteLine(list3[i]);
            //}
        }
    }
}
