--create InventoryDb318
--go
use InventoryDb318
go
select * from Selling

-- soal 16 tampilkan harga produk termurah perprovinsi
select pmhPro.namaProvinsi, pmhPro.namaProduk, cast(pmhPro.Harga as money)
from ProvMinHarga pmhPro
	join 
	(select 
		kodeProvinsi, namaProvinsi, min(Harga)Harga 
		from ProvMaxHarga
		group by kodeProvinsi, namaProvinsi
	) pmh
	on pmhPro.kodeProvinsi = pmh.kodeProvinsi and pmhPro.Harga = pmh.Harga

create view ProvMinHarga
as
select pr.Kode as 'KodeProvinsi', 
pr.Nama as 'namaProvinsi', 
p.Kode as 'kodeProduk', 
p.Nama as 'namaProduk', 
Min(p.Harga) Harga
from Provinsi pr
join kota k on pr.Kode = k.KodeProvinsi
join Outlet o on o.KodeKota = k.Kode
join Selling s on s.KodeOutlet = o.Kode
join Product p on s.KodeProduct = p.Kode
group by pr.Kode, pr.Nama, p.Kode, p.Nama

-- dibawah ratarata
select pmhPro.namaProvinsi, pmhPro.namaProduk, cast(pmhPro.Harga as money), pmh.harga Rata
from ProvRataHarga pmhPro
	join 
	(select 
		kodeProvinsi, namaProvinsi, avg(Harga)Harga 
		from ProvRataHarga
		group by kodeProvinsi, namaProvinsi
	) pmh
	on pmhPro.kodeProvinsi = pmh.kodeProvinsi and pmhPro.Harga < pmh.Harga
	order by pmhPro.kodeProvinsi

create view ProvRataHarga
as
select pr.Kode as 'KodeProvinsi', 
pr.Nama as 'namaProvinsi', 
p.Kode as 'kodeProduk', 
p.Nama as 'namaProduk', 
Avg(p.Harga) Harga
from Provinsi pr
join kota k on pr.Kode = k.KodeProvinsi
join Outlet o on o.KodeKota = k.Kode
join Selling s on s.KodeOutlet = o.Kode
join Product p on s.KodeProduct = p.Kode
group by pr.Kode, pr.Nama, p.Kode, p.Nama

--17. tampilkan outlet yg menjual (roti, pasta gigi dan seblak) dengan nama produk,quantity, harga & jumlah penjualan
select ou.Nama as namaOutlet,pr.Nama as namaProduk, sum(sel.Quantity) Quantity, pr.Harga, sum(Quantity * Harga) as [penjualan]
from Selling as sel
join Outlet as ou on sel.KodeOutlet = ou.Kode
join Product as pr on sel.KodeProduct = pr.Kode
join kota as kt on kt.Kode = ou.KodeKota
join provinsi as pro on pro.Kode = kt.KodeProvinsi 
where pr.Kode in ('P0192', 'P9289', 'P9830')
--where pr.Nama in('Roti', 'Pasta Gigi', 'Seblak')
group by ou.Nama, pr.Harga, pr.Nama

-- soal 18 Buat Nama untuk outlet Daerah Jakarta yang belum memiliki outlet
SELECT
	CONCAT(SUBSTRING(NamaKota, 1,3), SUBSTRING(NamaKota, CHARINDEX(' ',NamaKota,1) + 1,3), ' Jaya') NamaOutlet
FROM (
	SELECT
	Kota.Nama NamaKota
	FROM Outlet
	RIGHT JOIN Kota ON Outlet.KodeKota = Kota.Kode
	WHERE Outlet.Kode is null
	and Kota.Nama like '%Jakarta%'
) KotaNoOutlet

--soal group 1
--Note jumlah penjualan = qty * hrg, jumlah produk = qty
--1. Tampilkan hasil penjualan Outlet Jaktim Jaya berdasarkan hari di tahun 2022
-- bisa pakai ini cast(p.harga * s.quantity as int) as 'HasilPenjualan'
select day(s.SellingDate) as 'hari', o.Nama as 'nama_outlet', sum(s.Quantity * p.Harga)
from selling s
join outlet o on s.KodeOutlet = o.Kode
join Product p on s.KodeProduct = p.Kode
where o.Kode= 'O001' and year(s.SellingDate) = 2022
group by o.Kode, day( s.SellingDate), o.Nama

select * from outlet
-- pakai datename tampilin nama hari
--select DateName(dw, s.SellingDate) as 'hari', o.Nama as 'nama_outlet', sum(s.Quantity * p.Harga)
--from selling s
--join outlet o on s.KodeOutlet = o.Kode
--join Product p on s.KodeProduct = p.Kode
--where o.Nama = 'Jaktim Jaya' and year(s.SellingDate) = 2022
--group by DateName(dw, s.SellingDate), o.Nama

--2. Tampilkan nama produk & jumlah penjualan produk dari outlet Jakpus Jaya
select 
p.Nama 'namaProduk', 
cast(sum(s.Quantity * p.Harga)as money) as 'jumlah_penjualan', 
o.Nama as 'NamaOutlet' 
from selling s
join outlet o on s.KodeOutlet = o.Kode
join Product p on s.KodeProduct = p.Kode
where o.Kode = 'O005'
group by p.Nama, o.Nama

--3. Tampilkan nama produk yang terjual di bulan Januari
select 
DATENAME(month, s.SellingDate) as [bulan], 
p.Nama as 'NamaProduk' 
from Selling s
join Product p on s.KodeProduct = p.Kode
where month(s.SellingDate) = 01
group by DATENAME(month, s.SellingDate), p.Nama

-- SOAL GROUP 3 --
--1. Tampilkan Kode Produk, Nama Produk dan  jumlah produk diatas rata-rata yang nama produknya berakhiran i dan 
--    mengubah Nama Produk menjadi huruf kapital--

SELECT
	p.Kode, UPPER(p.Nama) NamaProduk,
	SUM(Quantity) JumlahTerjual
FROM
	dbo.Selling s
	JOIN dbo.Product p
		ON p.Kode = s.KodeProduct
GROUP BY
	p.Kode,
	p.Nama
HAVING
	SUM(Quantity) > (SELECT AVG(y.SumQuantity) FROM (SELECT KodeProduct, SUM(Quantity) 
	SumQuantity FROM dbo.Selling GROUP BY KodeProduct) y) AND P.Nama LIKE '%i'

--2. Tampilkan NamaProduk, Harga, dan JumlahTerjual, untuk produk yang belum pernah laku terjual

SELECT 
	p.Nama, 
	p.Harga, 
	ISNULL(SUM(sel.Quantity),0) JumlahTerjual
FROM Product p 
	LEFT JOIN Selling sel ON p.Kode = sel.KodeProduct
WHERE sel.Id  IS NULL
GROUP BY p.nama, p.harga
 
--3. Tampilkan NamaKota dan JumlahTransaksi, untuk kota yang belum pernah terjadi transaksi
SELECT 
	kt.Kode,
	kt.Nama NamaKota, 
	ISNULL(SUM(sel.Quantity),0) JumlahTransaksi
FROM Selling  sel
	RIGHT JOIN Outlet ou ON sel.KodeOutlet = ou.Kode
	RIGHT JOIN kota as kt ON kt.Kode = ou.KodeKota
	JOIN provinsi as pro ON pro.Kode = kt.KodeProvinsi 
--WHERE sel.Id IS NULL
GROUP BY kt.Kode, kt.Nama
HAVING SUM(sel.Quantity) IS NULL

SELECT 
	kt.Kode,
	kt.Nama NamaKota, 
	COUNT(sel.Referensi) JumlahTransaksi
FROM Selling  sel
	RIGHT JOIN Outlet ou ON sel.KodeOutlet = ou.Kode
	RIGHT JOIN kota as kt ON kt.Kode = ou.KodeKota
	JOIN provinsi as pro ON pro.Kode = kt.KodeProvinsi 
--WHERE sel.Id IS NULL
GROUP BY kt.Kode, kt.Nama
HAVING COUNT(sel.Referensi) > 0

-- group 2
-- Soal Mandiri

-- 1.  Tampilkan nama barang, harga barang dan tambahkan kolom dengan keterangan jika diatas rata-rata maka upper, jika dibawah rata2 maka lower

	SELECT
		Product.Nama NamaProduk,
		Product.Harga HargaProduk,
		CASE
			WHEN Product.Harga > 
				(SELECT
					AVG (Product.Harga)
				FROM
					Product) THEN 'Upper'
			WHEN Product.Harga < (
				SELECT
					AVG (Product.Harga)
				FROM
					Product) THEN 'Lower'
		END Keterangan
	FROM Product


-- 2.  Tampilkan Produk yang terjual di bulan dan hari yang sama

	SELECT
		Product.Nama Namaproduk
	FROM Selling
	JOIN Product ON Selling.KodeProduct = Product.Kode
	JOIN 
	(
		SELECT
			DAY (Selling.SellingDate) Hari,
			MONTH (Selling.SellingDate) Bulan
		FROM Selling
		GROUP BY Selling.SellingDate
		HAVING COUNT(*) > 1
	) SellDateDupe ON SellDateDupe.Hari = DAY(Selling.SellingDate) AND SellDateDupe.Bulan = MONTH(Selling.SellingDate)
	GROUP BY Product.Kode, Product.Nama

-- 3.  Tampilkan Frekuensi terjualnya suatu produk dengan menampilkan tanggal dan quantitynya

	SELECT
		Product.Nama NamaProduk,
		Selling.SellingDate Tanggal,
		COUNT(Product.Kode) Frekuensi,
		SUM(Selling.Quantity) JumlahBarangTerjual
	FROM Selling
	JOIN Product 
	ON Selling.KodeProduct = Product.Kode
	GROUP BY Product.Kode, Product.Nama, Selling.SellingDate
 
