create database DBPenerbit318
go
use DBPenerbit318
go

--1. Buatlah Tabel Pengarang : tblPengarang
create table tblPengarang(
	id int identity (1,1),
	kd_pengarang varchar(7) primary key not null,
	nama varchar(30) not null,
	alamat varchar(80) not null,
	kota varchar(15) not null,
	kelamin varchar(1) not null
)

--2. Buatlah view untuk attribute Kd_Pengarang, Nama, Kota dengan nama vwPengarang
create view vwPengarang
as 
select pgrg.kd_pengarang, pgrg.nama, pgrg.kota
from tblPengarang as pgrg 

--3. Masukan data dibawah kedalam table tblPengarang
insert into tblPengarang(kd_pengarang, nama, alamat, kota, kelamin)
values('P0001','Ashadi','Jl. Beo 25','Yogya','P'),
('P0002','Rian','Jl. Solo 123','Yogya','P'),
('P0003','Suwadi','Jl. Semangka 13','Bandung','P'),
('P0004','Siti','Jl. Durian 15','Solo','W'),
('P0005','Amir','Jl. Gajah 33','Kudus','P'),
('P0006','Suparman','Jl. Harimau 25','Jakarta','P'),
('P0007','Jaja','Jl. Singa 7','Bandung','P'),
('P0008','Saman','Jl. Naga 12','Yogya','P'),
('P0009','Anwar','Jl. Tidar 6A','Magelang','P'),
('P0010','Fatmawati','Jl. Renjana 4','Bogor','W')

--a. Tampilkan �Kd_Pengarang, nama� yang dikelompokan atas nama.
select p.kd_pengarang, nama from tblPengarang p order by p.nama asc

--b. Tampilkan �kota,Kd_Pengarang ,nama� yang dikelompokan atas kota.
select p.kota, p.kd_pengarang, p.nama from tblPengarang p

--c. Hitung dan tampilkan jumlah pengarang.
select count(p.kd_pengarang) as 'Jumlah_Pengarang' from tblPengarang p

--d. Tampilkan record kota dan jumlah kotanya.
select kota, count(kota) as jumlah_Kota 
from tblPengarang 
group by kota
order by kota asc

--e. Tampilkan record kota diatas 1 kota.
select kota, count(kota) as Record_Kota from tblPengarang group by kota having count(kota) > 1

--f. Tampilkan Kd_Pengarang yang terbesar dan terkecil.
select 
max(kd_pengarang) as Kode_Pengarang_Terbesar, 
MIN(kd_pengarang) as Kode_Pengarang_Terendah
from tblPengarang

--4. Buatlah tabel Gaji : tblGaji
create table tblGaji(
	ID int primary key identity (1,1),
	Kd_Pengarang varchar(7) not null,
	Nama varchar(30) not null,
	Gaji decimal(18,4) not null,
)

--5. Masukan data dibawah kedalam table tblGaji
insert into tblGaji values('P0002', 'Rian', 600000),
('P0005', 'Amir', 700000),
('P0004', 'Siti', 500000),
('P0003', 'Suwadi', 1000000),
('P0010', 'Fatmawati', 600000),
('P0008', 'Saman', 750000)

--a. Tampilkan gaji tertinggi dan terendah.
select max(gaji) as gaji_tertinggi, min(gaji) as gaji_terendah from tblGaji

--b. Tampilkan gaji diatas 600.000.
select nama, gaji from tblGaji where gaji > 600000

--c. Tampilkan jumlah gaji.
select sum(gaji) as Jumlah_Gaji from tblGaji

--d. Tampilkan jumlah gaji berdasarkan Kota
select kota,sum(gaji) as jumlah_gaji 
from tblGaji as gaji
join tblPengarang as pgrg 
on gaji.Kd_Pengarang = pgrg.kd_pengarang 
group by kota

--e. Tampilkan seluruh record pengarang antara P0001-P0006 dari tabel pengarang.
select * from tblPengarang where kd_pengarang between 'P0001' and 'P0006'

--f. Tampilkan seluruh data yogya, solo, dan magelang dari tabel pengarang.
select * from tblPengarang where kota in('yogya', 'solo', 'magelang')

--g. Tampilkan seluruh data yang bukan yogya dari tabel pengarang.
select * from tblPengarang where kota <> 'yogya'

--h. Tampilkan seluruh data pengarang yang nama (dapat digabungkan atau terpisah):
--a. dimulai dengan huruf [A]
select * from tblPengarang where nama LIKE 'a%'
--b. berakhiran [i]
select * from tblPengarang where nama LIKE '%i'
--c. huruf ketiganya [a]
select * from tblPengarang where nama LIKE '__a%'
--d. tidak berakhiran [n]
select * from tblPengarang where nama not like '%n'

--i. Tampilkan seluruh data table tblPengarang dan tblGaji dengan Kd_Pengarang yang sama
select *
from tblGaji as gaji
join tblPengarang as pgrg 
on gaji.Kd_Pengarang = pgrg.kd_pengarang 

--j. Tampilan kota yang memiliki gaji dibawah 1.000.000
select distinct kota
from tblGaji as gaji 
join tblPengarang as pgrg 
on gaji.Kd_Pengarang = pgrg.kd_pengarang 
where gaji < 1000000

--k. Ubah panjang dari tipe kelamin menjadi 10
alter table tblPengarang alter column kelamin varchar(10)

--l. Tambahkan kolom [Gelar] dengan tipe Varchar (12) pada tabel tblPengarang
alter table tblPengarang add gelar varchar(12) not null

--m. Ubah alamat dan kota dari Rian di table tblPengarang menjadi, Jl. Cendrawasih 65 dan Pekanbaru
update tblPengarang set 
alamat = 'Jl. Cendrawasih 65', 
kota = 'Pekanbaru' 
where nama = 'rian';

