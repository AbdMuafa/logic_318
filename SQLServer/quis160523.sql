--Soal SQL
use InventoryDb318

select * from Kota
select * from Outlet
select * from Product
select * from Provinsi
select * from Selling
--Note jumlah penjualan = qty * hrg, jumlah produk = qty
--01 Tapilkan jumlah penjualan barang peroutlet per-tanggal
select p.Nama as nama_barang, o.Nama as Nama_Outlet, s.SellingDate, sum(s.Quantity * p.Harga) as jumlah_penjualan from
Selling as s
join Outlet as o on o.Kode = s.KodeOutlet
join Product as p on p.Kode = s.KodeProduct
group by p.Nama, o.Kode, s.SellingDate, o.Nama
order by s.SellingDate

SELECT SellingDate, Quantity, KodeOutlet 
FROM Selling
GROUP BY Quantity, SellingDate, KodeOutlet
ORDER BY KodeOutlet
--02 Tapilkan jumlah penjualan per tahun
select YEAR(s.SellingDate) as [Tahun], sum(s.Quantity) as Jumlah_Penjualan
from Selling as s
group by YEAR(s.SellingDate)

--03 Tapilkan jumlah product terlaris dan ter tidak laris per kota
select  t1.nama_kota as 'Nama Kota', max(t1.Quantity) [Terlaris], min(t1.Quantity) [Tidak Terlaris] 
from (
		select s.KodeProduct, sum(s.Quantity)as 'Quantity', k.Nama as 'nama_kota'
		from Selling as s
		join Outlet as o on s.KodeOutlet = o.Kode
		join Kota as k on o.KodeKota = k.Kode
		join Product as p on p.Kode = s.KodeProduct
		group by s.KodeProduct, k.Nama, p.Nama
	) t1
group by t1.nama_kota

--cara ke 2
select  t1.nama_product as 'Nama Product', t1.nama_kota as 'Nama Kota', max(t1.Quantity) [Terlaris], min(t1.Quantity) [Tidak Terlaris] 
from (
		select s.KodeProduct, p.Nama as 'nama_product', sum(s.Quantity)as 'Quantity', k.Nama as 'nama_kota'
		from Selling as s
		join Outlet as o on s.KodeOutlet = o.Kode
		join Kota as k on o.KodeKota = k.Kode
		join Product as p on p.Kode = s.KodeProduct
		group by s.KodeProduct, k.Nama, p.Nama
	) t1
group by t1.nama_kota, t1.nama_product

--04 Tampilkan jumlah penjualan per provinsi dan urutkan dari yang terbesar
SELECT pr.Nama as 'Provinsi',SUM(Quantity * p.Harga) as 'Penjualan'
	FROM Selling s
	JOIN Product p on p.Kode = s.KodeProduct
	JOIN Outlet o on o.Kode = s.KodeOutlet
	JOIN Kota k on k.Kode = o.KodeKota
	JOIN Provinsi pr on pr.Kode = k.KodeProvinsi
	GROUP BY pr.kode, pr.Nama
	ORDER BY Penjualan DESC

--05 Tampilan referensi yang tidak sesuai dengan sellingdate
select Reference
from
(	select referensi reference, 
	YEAR(SellingDate) selYear, 
	MONTH(SellingDate) selMonth, 
	CAST(SUBSTRING(Referensi, 6,2)as int) dtRef,
	CAST(SUBSTRING(Referensi, 4,2) as int) yrRef
	from Selling
) s
where s.selMonth != s.dtRef and selYear <> yrRef

select * from selling
-- cara 2
select referensi reference, s.SellingDate
	from Selling as s
	where 
	CAST(SUBSTRING(Referensi, 4,2)as int) <>
	year(SellingDate) and
	CAST(SUBSTRING(Referensi, 4,2)as int) <>
	month(SellingDate)

--06 Tampilkan jumlah produk terjual pertahun peroutlet
select YEAR(s.SellingDate) Tahun, o.Kode kodeOutlet, o.Nama outletNama, sum(s.Quantity)
from Selling s
join Outlet o
on o.Kode = s.KodeOutlet
group by o.Kode, o.Nama, YEAR(s.SellingDate)


--07 Tampilan jumlah penjualan peroutlet
select o.Kode as 'kode_outlet',o.Nama as 'nama_outlet', sum(s.Quantity * p.Harga)as 'jumlah'
		from Selling as s
		join Outlet as o on s.KodeOutlet = o.Kode
		join Product as p on p.Kode = s.KodeProduct
		group by o.Kode, o.Nama
order by jumlah DESC
--08 Tampilan jumlah penjualan per bulan diurutkan berdasar bulan
select MONTH(s.SellingDate) 'Bulan', sum(s.Quantity * p.Harga) 'Penjualan'
from Selling s
join Product p on s.KodeProduct = p.Kode
group by MONTH(s.SellingDate)
order by MONTH(s.SellingDate)

-- cara ke 2
SELECT
	YEAR(sel.sellingdate) Periode,
	DATENAME(month, sel.sellingdate) Bulan,
	SUM(sel.quantity*pro.Harga) JumlahPenjualan
FROM selling sel
JOIN Product pro ON pro.kode = sel.KodeProduct
GROUP BY YEAR(sel.sellingdate), DATENAME(month, sel.sellingdate),MONTH(sel.sellingdate)
ORDER BY YEAR(sel.sellingdate), MONTH(sel.sellingdate)

--09 Tampilan rata-rata jumlah penjualan setiap bulan
select 
    avg(Quantity* harga) as 'Rata- rata Jumlah Penjualan',  
    month(SellingDate) as 'Bulan'
	--year(sellingDate) as 'Tahun'
from Selling as sel
join Outlet as ou on sel.KodeOutlet = ou.Kode
join Product as pr on sel.KodeProduct = pr.Kode
group by month(SellingDate)

--10 Tampilan produk dengan jumlah produk dibawah rata-rata
SELECT
	p.Nama NamaProduk,
	sum(Quantity) JumlahTerjual
FROM
	Selling s
	JOIN Product p
		ON p.Kode = s.KodeProduct
GROUP BY
	p.Kode,
	p.Nama
HAVING
	sum(Quantity) < (SELECT 
						avg(y.SumQuantity) 
					FROM 
						(SELECT 
							KodeProduct, 
							sum(Quantity) SumQuantity 
						FROM Selling 
						GROUP BY KodeProduct) 
					y)

--11 Tampilan jumlah outlet per provinsi
select pr.Nama as 'Nama Provinsi', count(o.Nama) as 'Jumlah Outlet'
from Outlet o
left join Kota k on o.KodeKota = k.Kode
left join Provinsi pr on pr.Kode = k.KodeProvinsi
group by pr.Nama

--12 Tampilan produk terlaris per perionde bulanan
select selQty.*
from selQty
join (select [Year], [Month] ,MAX(Quantity) Quantity
from selQty
group by [Year],[Month]) selmaxQty
on selQty.Quantity = selmaxQty.Quantity and selQty.Year = 
selmaxQty.Year and selQty.Month = selmaxQty.Month
order by selQty.Year, selQty.Month
go

create view selQty
as select year(s.SellingDate)[Year], MONTH(s.SellingDate)[Month], s.KodeProduct, SUM(s.Quantity) as 'Quantity'
from selling as s
group by year(s.SellingDate), MONTH(s.SellingDate), s.KodeProduct
go

--13 Tampilkan provinsi yg menjual sampoo, quantity, harga & jumlah penjualan
select p.Nama, pr.Nama, sum(s.Quantity), pr.Harga, sum(pr.Harga * s.Quantity) as 'Jumlah_Penjualan'
from Provinsi p
join kota k on p.Kode = k.KodeProvinsi
join Outlet o on o.KodeKota = k.Kode
join Selling s on s.KodeOutlet = o.Kode
join Product pr on pr.Kode = s.KodeProduct
where pr.Nama like '%sampoo%'
group by p.Nama, pr.Nama, pr.Harga

--14 Tampilkan produk harga termahal setiap provinsi
select pmhPro.namaProvinsi, pmhPro.namaProduk, pmhPro.Harga
from ProvMaxHarga pmhPro
join 
(select kodeProvinsi, namaProvinsi, max(Harga)Harga 
from ProvMaxHarga
group by kodeProvinsi, namaProvinsi) pmh
on pmhPro.kodeProvinsi = pmh.kodeProvinsi and pmhPro.Harga = pmh.Harga

create view ProvMaxHarga
as
select pr.Kode as 'KodeProvinsi', pr.Nama as 'namaProvinsi', p.Kode as 'kodeProduk', p.Nama as 'namaProduk', MAX(p.Harga) Harga
from Provinsi pr
join kota k on pr.Kode = k.KodeProvinsi
join Outlet o on o.KodeKota = k.Kode
join Selling s on s.KodeOutlet = o.Kode
join Product p on s.KodeProduct = p.Kode
group by pr.Kode, pr.Nama, p.Kode, p.Nama

--15 Tampilkan outlet dengan penjualan tertinggi & terendah
--mencari min
select MIN(Penjualan)
from
(
	SELECT
		o.Kode KodeO,
		o.Nama NamaO,
		--prod.Kode KodeP,
		isnull(sum(s.Quantity * prod.Harga), 0) Penjualan
	FROM dbo.Selling s
	RIGHT JOIN dbo.Outlet o
		ON o.Kode = s.KodeOutlet
	LEFT JOIN dbo.Product prod
		ON prod.Kode = s.KodeProduct
	GROUP BY
		o.Kode, o.Nama) Mino

--cara yg panjang
SELECT
		CASE
			WHEN sm.Penjualan = (SELECT * FROM max15) THEN 'Maksimal'
			WHEN sm.Penjualan = (SELECT * FROM min15) THEN 'Minimal'
		END Stat,
		sm.NamaO NamaOutlet,
		sm.Penjualan
	FROM (
		SELECT
			o.Kode KodeO,
			o.Nama NamaO,
			--prod.Kode KodeP,
			SUM(s.Quantity * prod.Harga) Penjualan
		FROM dbo.Selling s
		JOIN dbo.Outlet o
			ON o.Kode = s.KodeOutlet
		JOIN dbo.Product prod
			ON prod.Kode = s.KodeProduct
		GROUP BY
			o.Kode,
			o.Nama
	) sm
	WHERE
		sm.Penjualan = (SELECT * FROM max15) OR
		sm.Penjualan = (SELECT * FROM min15)
		--sm.Penjualan = @max OR
		--sm.Penjualan = @min

CREATE VIEW max15
AS
	SELECT
		MAX(mx.Penjualan) Penjualan
	FROM (
		SELECT
			o.Kode KodeO,
			--prod.Kode KodeP,
			SUM(s.Quantity * prod.Harga) Penjualan
		FROM dbo.Selling s
		JOIN dbo.Outlet o
			ON o.Kode = s.KodeOutlet
		JOIN dbo.Product prod
			ON prod.Kode = s.KodeProduct
		GROUP BY
			o.Kode
	) mx

CREATE VIEW min15
AS
	SELECT
		MIN(mn.Penjualan) Penjualan
	FROM (
		SELECT
			o.Kode KodeO,
			--prod.Kode KodeP,
			SUM(s.Quantity * prod.Harga) Penjualan
		FROM dbo.Outlet o
		LEFT JOIN dbo.Selling s
			ON o.Kode = s.KodeOutlet
		LEFT JOIN dbo.Product prod
			ON prod.Kode = s.KodeProduct
		GROUP BY
			o.Kode
	) mn


select * from Kota
select * from Outlet
select * from Product
select * from Provinsi
select * from Selling

-- soal 16 tampilkan harga produk termurah perprovinsi
select pmhPro.namaProvinsi, pmhPro.namaProduk, pmhPro.Harga
from ProvMaxHarga pmhPro
join 
(select kodeProvinsi, namaProvinsi, min(Harga)Harga 
from ProvMaxHarga
group by kodeProvinsi, namaProvinsi) pmh
on pmhPro.kodeProvinsi = pmh.kodeProvinsi and pmhPro.Harga = pmh.Harga

create view ProvMinHarga
as
select pr.Kode as 'KodeProvinsi', pr.Nama as 'namaProvinsi', p.Kode as 'kodeProduk', p.Nama as 'namaProduk', Min(p.Harga) Harga
from Provinsi pr
join kota k on pr.Kode = k.KodeProvinsi
join Outlet o on o.KodeKota = k.Kode
join Selling s on s.KodeOutlet = o.Kode
join Product p on s.KodeProduct = p.Kode
group by pr.Kode, pr.Nama, p.Kode, p.Nama

--17. tampilkan outlet yg menjual (roti, pasta gigi dan seblak) dengan nama produk,quantity, harga & jumlah penjualan
select ou.Nama as namaOutlet,pr.Nama as namaProduk, sum(sel.Quantity) Quantity, pr.Harga, sum(Quantity * Harga) as [penjualan]
from Selling as sel
join Outlet as ou on sel.KodeOutlet = ou.Kode
join Product as pr on sel.KodeProduct = pr.Kode
join kota as kt on kt.Kode = ou.KodeKota
join provinsi as pro on pro.Kode = kt.KodeProvinsi 
where pr.Nama in('Roti', 'Pasta Gigi', 'Seblak')
group by ou.Nama, pr.Harga, pr.Nama

-- soal 18 Buat Nama untuk outlet Daerah Jakarta yang belum memiliki outlet
SELECT
	CONCAT(SUBSTRING(NamaKota, 1,3), SUBSTRING(NamaKota, CHARINDEX(' ',NamaKota,1) + 1,3), ' Jaya') NamaOutlet
FROM (
	SELECT
	Kota.Nama NamaKota
	FROM Outlet
	RIGHT JOIN Kota ON Outlet.KodeKota = Kota.Kode
	WHERE Outlet.Kode is null
	and Kota.Nama like '%Jakarta%'
) KotaNoOutlet
